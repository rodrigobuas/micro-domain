# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.20](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.19...v0.0.20) (2022-03-07)

### [0.0.19](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.18...v0.0.19) (2022-02-08)

### [0.0.18](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.17...v0.0.18) (2021-12-15)

### [0.0.17](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.16...v0.0.17) (2021-10-22)

### [0.0.16](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.15...v0.0.16) (2021-10-22)

### [0.0.15](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.14...v0.0.15) (2021-10-14)

### [0.0.14](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.13...v0.0.14) (2021-07-21)

### [0.0.13](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.12...v0.0.13) (2021-07-20)

### [0.0.12](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.11...v0.0.12) (2021-05-27)

### [0.0.11](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.10...v0.0.11) (2021-05-12)

### [0.0.10](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.9...v0.0.10) (2021-05-11)

### [0.0.9](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.8...v0.0.9) (2021-05-11)

### [0.0.8](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.7...v0.0.8) (2021-04-28)

### [0.0.7](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.6...v0.0.7) (2021-04-28)

### [0.0.6](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.5...v0.0.6) (2021-04-13)

### [0.0.5](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.4...v0.0.5) (2021-04-13)


### Features

* add domain hooks useAction ([cb8e833](https://gitlab.com/rodrigobuas/micro-domain/commit/cb8e833c5d488a019eaee873509b93f40398812e))
* change piwik endpoints ([6bcd56b](https://gitlab.com/rodrigobuas/micro-domain/commit/6bcd56b3c15caa3cd3addf2d2f3a5366c012b35c))

### [0.0.4](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.3...v0.0.4) (2021-04-07)

### [0.0.3](https://gitlab.com/rodrigobuas/micro-domain/compare/v0.0.2...v0.0.3) (2021-04-07)

### 0.0.2 (2021-04-07)


### Features

* add DomainStore class ([374a62a](https://gitlab.com/rodrigobuas/micro-domain/commit/374a62adf949c1717c3ebe58512150f8171ef8e7))
* add EnvConfig class ([19ca11c](https://gitlab.com/rodrigobuas/micro-domain/commit/19ca11cd15595dd689ae20fcb16f97942fedec25))
* add EventTracker class ([f7bf7f1](https://gitlab.com/rodrigobuas/micro-domain/commit/f7bf7f18e2223bb05c7fef01c97c3278721944d3))
* add RestClient class ([9cc9fef](https://gitlab.com/rodrigobuas/micro-domain/commit/9cc9fefa6b84dfadc691d6a1e9e27de140d9ccc1))
* add rights module ([43fd1d2](https://gitlab.com/rodrigobuas/micro-domain/commit/43fd1d277e811ccf6072a39035c2c760b1872dd0))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
