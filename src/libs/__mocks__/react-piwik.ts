/* eslint-disable @typescript-eslint/no-empty-function */
// ./__mocks__/react-piwik.js

export default class ReactPiwik {
  static push(): void {}
  connectToHistory(): void {}
  disconnectFromHistory(): void {}
  track(): void {}
}
