import { Reducer } from 'redux';
import { Saga } from 'redux-saga';

export interface ActionConfig {
  payload?: (...args: any[]) => Record<string, unknown>;
  reducer?: Reducer;
  saga?: Saga;
}

export interface DomainActions {
  [actionType: string]: ActionConfig;
}
