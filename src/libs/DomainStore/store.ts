import {
  combineReducers,
  Store,
  createStore,
  compose,
  applyMiddleware,
  DeepPartial,
  Reducer,
  StoreEnhancer,
} from 'redux';
import createSagaMiddleware, { Saga } from 'redux-saga';
import { routerMiddleware, connectRouter } from 'connected-react-router/immutable';
import { History, createBrowserHistory } from 'history';

interface Window {
  __REDUX_DEVTOOLS_EXTENSION__?: () => any;
}

function withReduxDevTools(middlewares: StoreEnhancer<{ dispatch: any }>[]) {
  const reduxDevtools = (window as Window).__REDUX_DEVTOOLS_EXTENSION__;
  if (!reduxDevtools) {
    return middlewares;
  }

  return [...middlewares, reduxDevtools()];
}

export interface StoreConfig<RootState> {
  preloadedState?: DeepPartial<RootState>;
  history?: History;
  reducers?: Record<string, Reducer>;
  sagas?: Saga[];
}

function createRootReducer(history: History, reducers: Record<string, Reducer>) {
  return combineReducers({
    router: connectRouter(history),
    ...reducers,
  });
}

export function buildStore<RootState>({
  preloadedState = {},
  history = createBrowserHistory(),
  reducers = {},
  sagas = [],
}: StoreConfig<unknown>): Store<RootState> {
  /* configure store domains reducers */
  const rootReducer = createRootReducer(history, reducers);

  /* configure store middlewares */
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = withReduxDevTools([applyMiddleware(routerMiddleware(history), sagaMiddleware)]);

  /* create store */
  const appStore: Store<RootState> = createStore(rootReducer, preloadedState, compose(...middlewares));

  /* run sagas */
  sagas.forEach((saga: Saga) => sagaMiddleware.run(saga));

  return appStore;
}
