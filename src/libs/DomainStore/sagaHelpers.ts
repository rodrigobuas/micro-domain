import { put, PutEffect } from '@redux-saga/core/effects';
import { AnyAction } from 'redux';

type GeneratorPutEffect<P extends AnyAction> = Generator<PutEffect<P>>;

export function* startSaga(action: AnyAction): GeneratorPutEffect<{ type: string }> {
  const { type } = action;
  yield put({ ...action, type: `${type}/START` });
}

export function* endSagaWithSuccess(
  action: AnyAction,
  response: unknown,
): GeneratorPutEffect<{ type: string; response: any }> {
  const { type } = action;
  yield put({ ...action, type: `${type}/SUCCESS`, response });
}

export function* endSagaWithError(action: AnyAction, error: Error): GeneratorPutEffect<{ type: string; error: Error }> {
  const { type } = action;
  yield put({ ...action, type: `${type}/ERROR`, error });
}
