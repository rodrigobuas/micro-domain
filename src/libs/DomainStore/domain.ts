import { Reducer, AnyAction } from 'redux';
import { takeEvery, put, call } from '@redux-saga/core/effects';
import { Saga, SagaIterator } from 'redux-saga';
import forEach from 'lodash.foreach';

import { DomainActions } from './domain.types';
import { useDispatch } from 'react-redux';

const makeStartActionType = (actionType: string): string => `${actionType}/START`;
const makeSuccessActionType = (actionType: string): string => `${actionType}/SUCCESS`;
const makeErrorActionType = (actionType: string): string => `${actionType}/ERROR`;

interface SagaControllers {
  start?: Reducer;
  success?: Reducer;
  error?: Reducer;
}

function buildSagaControllerActions(actions: DomainActions, sagaControllers?: SagaControllers): DomainActions {
  if (!actions || !sagaControllers) {
    return {};
  }

  const { start, success, error } = sagaControllers;

  const controllers: DomainActions = {};
  forEach(actions, (_action, actionType) => {
    if (start) {
      controllers[makeStartActionType(actionType)] = { reducer: start };
    }
    if (success) {
      controllers[makeSuccessActionType(actionType)] = { reducer: success };
    }
    if (error) {
      controllers[makeErrorActionType(actionType)] = { reducer: error };
    }
  });

  return controllers;
}

function makeSagaController(saga: Saga): Saga {
  return function* sagaControl(action: AnyAction) {
    try {
      yield put({ ...action, type: makeStartActionType(action.type) });

      const response = yield call(saga, action);

      yield put({ ...action, type: makeSuccessActionType(action.type), response });
    } catch (error) {
      yield put({ ...action, type: makeErrorActionType(action.type), error: error && error.toString() });
    }
  };
}

export interface DomainOptions<DomainState> {
  initialState?: DomainState;
  actions?: DomainActions;
  sagaControllers?: SagaControllers;
}

export class Domain<DomainState> {
  public initialState: DomainState;
  public actions: DomainActions;
  public controllerActions: DomainActions;

  constructor({ initialState, actions, sagaControllers }: DomainOptions<DomainState>) {
    this.initialState = initialState;
    this.actions = actions;
    this.controllerActions = buildSagaControllerActions(actions, sagaControllers);
  }

  action(actionType: string, ...payloadArgs: any[]): AnyAction {
    const { actions } = this;
    const actionConfig = actions[actionType];
    if (!actionConfig) {
      throw new Error(`Missing action config for action type '${actionType}'`);
    }

    const payloadCreator = actionConfig.payload;
    const payload = payloadCreator ? payloadCreator(...payloadArgs) : {};
    return { type: actionType, payload };
  }

  useAction(actionType: string): (...args: any[]) => void {
    const dispatch = useDispatch();
    return (...payloadArgs: any[]) => dispatch(this.action(actionType, ...payloadArgs));
  }

  makeReducer(): Reducer<DomainState> {
    const { initialState, actions, controllerActions } = this;

    return (state: DomainState = initialState, action: AnyAction) => {
      const actionConfig = actions[action.type] || controllerActions[action.type];
      if (!actionConfig) {
        return state;
      }

      const { reducer } = actionConfig;
      if (!reducer) {
        return state;
      }

      return reducer(state, action);
    };
  }

  makeSaga(): Saga {
    const { actions } = this;
    return function* domainSaga(): SagaIterator {
      for (const actionType in actions) {
        const actionConfig = actions[actionType];
        if (!actionConfig || !actionConfig.saga) {
          continue;
        }

        yield takeEvery(actionType, makeSagaController(actionConfig.saga));
      }
    };
  }
}
