import { compose } from 'redux';

import { buildStore } from '../store';

(window as any).__REDUX_DEVTOOLS_EXTENSION__ = compose;

describe('domainStore', () => {
  describe('store', () => {
    it('should verify buildStore creation', () => {
      // given
      interface StateTest {
        router: any;
      }
      const store = buildStore<StateTest>({});
      const location = store.getState().router.toJS();

      // then
      expect(location).toMatchObject({
        location: {
          hash: '',
          pathname: '/',
          search: '',
          state: undefined,
        },
      });
    });
  });
});
