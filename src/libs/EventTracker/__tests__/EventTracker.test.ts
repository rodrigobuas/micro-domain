import ReactPiwik from 'react-piwik';

import { EventTracker } from '../EventTracker';

describe('EventTracker', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('constructor', () => {
    it('should instantiate an EventTracker', () => {
      // when
      const eventTracker = new EventTracker({ siteId: 'neo', analytcsUrl: 'matrix.com' });

      // then
      expect(eventTracker).toBeDefined();
      expect(eventTracker.tracker).toBeDefined();
    });
  });

  describe('trackUser', () => {
    it('should track an user', () => {
      // give
      const eventTracker = new EventTracker({ siteId: 'neo', analytcsUrl: 'matrix.com' });
      const spyPush = jest.spyOn(ReactPiwik, 'push');

      // when
      eventTracker.trackUser('neo');

      // then
      expect(spyPush).toHaveBeenCalledTimes(1);
      expect(spyPush).toHaveBeenCalledWith(['setUserId', 'neo']);
    });

    it('should track an user with info', () => {
      // give
      const eventTracker = new EventTracker({ siteId: 'neo', analytcsUrl: 'matrix.com' });
      const spyPush = jest.spyOn(ReactPiwik, 'push');

      // when
      eventTracker.trackUser('neo', { name: 'Anderson' });

      // then
      expect(spyPush).toHaveBeenCalledTimes(2);
    });
  });

  describe('trackEvent', () => {
    it('should track an event', () => {
      // give
      const eventTracker = new EventTracker({ siteId: 'neo', analytcsUrl: 'matrix.com' });
      const spyPush = jest.spyOn(ReactPiwik, 'push');

      // when
      eventTracker.trackEvent('pill', 'choose', 'neo');

      // then
      expect(spyPush).toHaveBeenCalledTimes(1);
      expect(spyPush).toHaveBeenCalledWith(['trackEvent', 'pill', 'choose', 'neo']);
    });

    it('should track an event with value', () => {
      // give
      const eventTracker = new EventTracker({ siteId: 'neo', analytcsUrl: 'matrix.com' });
      const spyPush = jest.spyOn(ReactPiwik, 'push');

      // when
      eventTracker.trackEvent('pill', 'choose', 'neo', 'red');

      // then
      expect(spyPush).toHaveBeenCalledTimes(1);
      expect(spyPush).toHaveBeenCalledWith(['trackEvent', 'pill', 'choose', 'neo', 'red']);
    });
  });
});
