import { History } from 'history';
import forEach from 'lodash.foreach';
import ReactPiwik from 'react-piwik';

interface EventTrackerProperties {
  siteId: string | number | boolean;
  analytcsUrl: string | null;
  trackErrors?: boolean;
}

interface ReactPiwikInterface {
  connectToHistory: (history: History) => unknown;
  disconnectFromHistory: () => unknown;
  track: (loc: Location) => unknown;
  push: (args: Array<string | number>) => unknown;
}

export class EventTracker {
  public tracker: ReactPiwikInterface;

  constructor({ siteId, analytcsUrl, trackErrors = true }: EventTrackerProperties) {
    this.tracker = new ReactPiwik({
      url: analytcsUrl,
      siteId,
      trackErrors,
      clientTrackerName: 'matomo.js',
      serverTrackerName: 'matomo.php',
    });
  }

  trackUser(id: string, info?: Record<string, string>): void {
    ReactPiwik.push(['setUserId', id]);

    if (info) {
      forEach(info, (key, value) => {
        ReactPiwik.push(['setCustomVariable', 1, key, value, 'visit']);
      });
    }
  }

  trackEvent(category: string, action: string, name: string, value?: string): void {
    const info = ['trackEvent', category, action, name];
    if (value !== undefined) {
      info.push(value);
    }
    ReactPiwik.push(info);
  }

  connectToHistory(history: History): unknown {
    this.tracker.track(window.location);
    return this.tracker.connectToHistory(history);
  }
}
