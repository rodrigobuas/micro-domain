import { asyncStep } from '../../../__tests__/helpers';
import { repositoryDomain } from '../repositoryDomain.domain';
import {
  REPOSITORY_FETCH,
  REPOSITORY_CREATE,
  REPOSITORY_REMOVE,
  REPOSITORY_UPDATE,
  REPOSITORY_FETCH_SELECTED,
  REPOSITORY_UPDATE_SELECTED,
  REPOSITORY_UPDATE_LIST,
} from '../repositoryDomain.types';
import { createRepositoryStore, getListFromStore, axiosMock, getSelectedFromStore } from './repositoryDomain.helpers';
import { regionsMocked, newRegionMocked, regionMocked, financialMocked } from '../__mocks__/repositoryDomain.mocks';

const URL_API = 'my-api/my-url/search';

const region = regionsMocked.data[0];
const regions = regionsMocked.data;

describe('Repository.domain', () => {
  describe('action.REPOSITORY_FETCH', () => {
    beforeEach(() => axiosMock.reset());

    it('should fetch regions List from api', async () => {
      // given
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionsMocked);

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);
    });

    it('should catch error when fetching regions list from api fails', async () => {
      // given
      console.error = jest.fn();
      const { store } = createRepositoryStore();

      axiosMock.onGet(`/regions`).networkError();

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_CREATE', () => {
    afterEach(() => axiosMock.reset());

    it('should create region on api', async () => {
      // given (fetch)
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionsMocked);
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      expect(getListFromStore(store, 'regions')).toBeUndefined();
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

      // when
      axiosMock.onPost(URL_API, newRegionMocked).reply(201, newRegionMocked);

      store.dispatch(
        repositoryDomain.action(REPOSITORY_CREATE, { endpoint: URL_API, key: 'regions', data: newRegionMocked }),
      );

      // then
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(4);
    });
    it('should catch error when creating region from api fails', async () => {
      // given
      const { store } = createRepositoryStore();

      axiosMock.onPost(URL_API, newRegionMocked).networkError();

      // when
      store.dispatch(
        repositoryDomain.action(REPOSITORY_CREATE, { endpoint: URL_API, key: 'regions', data: newRegionMocked }),
      );
      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_UPDATE', () => {
    afterEach(() => axiosMock.reset());

    it('should update region on api', async () => {
      // given (fetch)
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionsMocked);
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      expect(getListFromStore(store, 'regions')).toBeUndefined();
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

      // when
      axiosMock.onPatch('/regions', region).reply(201, region);

      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE, { endpoint: URL_API, key: 'regions', data: newRegionMocked }),
      );

      // then
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);
    });
    it('should catch error when updating region from api fails', async () => {
      // given
      const { store } = createRepositoryStore();

      axiosMock.onPatch(URL_API, newRegionMocked).networkError();

      // when
      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE, { endpoint: URL_API, key: 'regions', data: newRegionMocked }),
      );
      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_UPDATE_LIST', () => {
    afterEach(() => axiosMock.reset());

    it('should update regions on api', async () => {
      // given (fetch)
      const regionsEdited = [
        {
          id: 'AM',
          name: 'America uwu',
        },
        {
          id: 'EA',
          name: 'Eurasia uwu',
        },
      ];
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionsMocked);
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      expect(getListFromStore(store, 'regions')).toBeUndefined();
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

      // when
      axiosMock.onPatch('/regions', regionsEdited).reply(201, regionsEdited);

      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE_LIST, { endpoint: '/regions', key: 'regions', data: regionsEdited }),
      );

      // then
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);
    });
    it('should catch error when updating region from api fails', async () => {
      // given
      const { store } = createRepositoryStore();

      axiosMock.onPatch(URL_API, regions).networkError();

      // when
      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE_LIST, { endpoint: '/regions', key: 'regions', data: regions }),
      );
      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_REMOVE', () => {
    afterEach(() => axiosMock.reset());

    it('should remove region on api', async () => {
      // given (fetch)
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionsMocked);
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH, { endpoint: URL_API, key: 'regions' }));

      expect(getListFromStore(store, 'regions')).toBeUndefined();
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

      // when
      axiosMock.onDelete(`${URL_API}/${region.id}`).reply(204);

      store.dispatch(repositoryDomain.action(REPOSITORY_REMOVE, { endpoint: URL_API, key: 'regions', id: region.id }));

      // then
      await asyncStep();
      expect(getListFromStore(store, 'regions').toJS()).toHaveLength(2);
    });

    it('should catch error when deleting region from api fails', async () => {
      // given
      const { store } = createRepositoryStore();

      axiosMock.onDelete(`${URL_API}/${region.id}`).networkError();

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_REMOVE, { endpoint: URL_API, key: 'regions', id: region.id }));
      // then
      expect(getListFromStore(store, 'regions')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_FETCH_SELECTED', () => {
    beforeEach(() => axiosMock.reset());

    it('should fetch region one region from api', async () => {
      // given
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionMocked);

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH_SELECTED, { endpoint: URL_API, key: 'region' }));

      // then
      expect(getSelectedFromStore(store, 'region')).toBeUndefined();
      await asyncStep();
      expect(getSelectedFromStore(store, 'region')).toBeDefined();
    });

    it('should fetch financials data from api', async () => {
      // given
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, financialMocked);

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH_SELECTED, { endpoint: URL_API, key: 'financial' }));

      // then
      expect(getSelectedFromStore(store, 'financial')).toBeUndefined();
      await asyncStep();
      expect(getSelectedFromStore(store, 'financial')).toBeDefined();
    });

    it('should catch error when fetching region from api fails', async () => {
      // given
      console.error = jest.fn();
      const { store } = createRepositoryStore();

      axiosMock.onGet(`/region`).networkError();

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH_SELECTED, { endpoint: URL_API, key: 'region' }));

      // then
      expect(getListFromStore(store, 'region')).toBeUndefined();
    });
  });

  describe('action.REPOSITORY_UPDATE_SELECTED', () => {
    afterEach(() => axiosMock.reset());

    it('should update region selected on api', async () => {
      // given (fetch)
      const { store } = createRepositoryStore();
      axiosMock.onGet(URL_API).reply(200, regionMocked);

      // when
      store.dispatch(repositoryDomain.action(REPOSITORY_FETCH_SELECTED, { endpoint: URL_API, key: 'region' }));

      // then
      expect(getSelectedFromStore(store, 'region')).toBeUndefined();
      await asyncStep();
      expect(getSelectedFromStore(store, 'region')).toBeDefined();

      // when
      axiosMock.onPatch('/region', region).reply(201, region);

      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE_SELECTED, {
          endpoint: URL_API,
          key: 'region',
          data: newRegionMocked,
        }),
      );

      // then
      await asyncStep();
      expect(getSelectedFromStore(store, 'region')).toBeDefined();
    });

    it('should catch error when updating region from api fails', async () => {
      // given
      const { store } = createRepositoryStore();

      axiosMock.onPatch(URL_API, newRegionMocked).networkError();

      // when
      store.dispatch(
        repositoryDomain.action(REPOSITORY_UPDATE_SELECTED, {
          endpoint: URL_API,
          key: 'region',
          data: newRegionMocked,
        }),
      );
      // then
      expect(getSelectedFromStore(store, 'region')).toBeUndefined();
    });
  });
});
