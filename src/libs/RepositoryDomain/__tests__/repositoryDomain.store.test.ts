import { repositoryDomain } from '../repositoryDomain.domain';
import {
  REPOSITORY_SET_LIST,
  REPOSITORY_DELETE,
  REPOSITORY_ADD,
  REPOSITORY_EDIT,
  REPOSITORY_SET_SELECTED,
} from '../repositoryDomain.types';
import {
  createRepositoryStore,
  getListFromStore,
  getRepositoryFromStore,
  getMetaFromStore,
} from './repositoryDomain.helpers';
import { regionsMocked, newRegionMocked, regionMocked } from '../__mocks__/repositoryDomain.mocks';
import { getItem, getSelected } from '../repositoryDomain.store';

const dataRegionsMocked = regionsMocked.data;

describe('Repository.reducer', () => {
  it('should setRegions into the store', () => {
    // given
    const { store, dispatchSpy } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    // then
    expect(dispatchSpy).toHaveBeenCalledWith({
      type: REPOSITORY_SET_LIST,
      payload: { list: dataRegionsMocked, key: 'regions' },
    });
  });

  it('should delete region into the store', () => {
    // given
    const { store, dispatchSpy } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    store.dispatch(repositoryDomain.action(REPOSITORY_DELETE, { id: dataRegionsMocked[0], key: 'regions' }));
    // then
    expect(dispatchSpy).toHaveBeenCalledWith({
      type: REPOSITORY_DELETE,
      payload: { id: dataRegionsMocked[0], key: 'regions' },
    });
  });

  it('should not delete region , if dont find index', () => {
    // given
    const { store } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    const initialState = getListFromStore(store, 'regions');

    store.dispatch(repositoryDomain.action(REPOSITORY_DELETE, { id: 'NOT_EXIST', key: 'regions' }));

    // then
    expect(getListFromStore(store, 'regions')).toMatchObject(initialState);
  });

  it('should add region into the store', () => {
    // given
    const { store, dispatchSpy } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    store.dispatch(repositoryDomain.action(REPOSITORY_ADD, { data: newRegionMocked, key: 'regions' }));
    // then
    expect(dispatchSpy).toHaveBeenCalledWith({
      type: REPOSITORY_ADD,
      payload: { data: newRegionMocked, key: 'regions' },
    });
  });

  it('should edit region into the store', () => {
    // given
    const { store, dispatchSpy } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    store.dispatch(repositoryDomain.action(REPOSITORY_EDIT, { data: dataRegionsMocked[0], key: 'regions' }));
    // then
    expect(dispatchSpy).toHaveBeenCalledWith({
      type: REPOSITORY_EDIT,
      payload: { data: dataRegionsMocked[0], key: 'regions' },
    });
  });

  it('should not edit region, if dont find index', () => {
    // given
    const { store } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    const initialState = getListFromStore(store, 'regions');

    store.dispatch(repositoryDomain.action(REPOSITORY_EDIT, { data: {}, key: 'regions' }));
    // then
    expect(getListFromStore(store, 'regions')).toMatchObject(initialState);
  });

  it('should get meta region into the store', () => {
    // given
    const { store } = createRepositoryStore();
    // when
    store.dispatch(
      repositoryDomain.action(REPOSITORY_SET_LIST, {
        list: dataRegionsMocked,
        key: 'regions',
        meta: { total: regionsMocked.total, limit: regionsMocked.limit },
      }),
    );
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    const meta = getMetaFromStore(store, 'regions');

    // then
    expect(meta).toMatchObject({ total: regionsMocked.total, limit: regionsMocked.limit });
  });

  it('should get one region into the store', () => {
    // given
    const { store } = createRepositoryStore();

    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    const region = getItem(getRepositoryFromStore(store), 'regions', 'id', 'AM');
    // then
    expect(region).toMatchObject(dataRegionsMocked[0]);
  });

  it('should get one region into the store return an error', () => {
    // given
    const { store } = createRepositoryStore();
    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_LIST, { list: dataRegionsMocked, key: 'regions' }));
    expect(getListFromStore(store, 'regions').toJS()).toHaveLength(3);

    const region = getItem(getRepositoryFromStore(store), 'regions', 'id', 'AAAAAAAAA');
    // then
    expect(region).toBeNull();
  });

  it('should get one region into the selected store', () => {
    // given
    const { store } = createRepositoryStore();

    // when
    store.dispatch(repositoryDomain.action(REPOSITORY_SET_SELECTED, { entity: regionMocked.data[0], key: 'region' }));

    const region = getSelected(getRepositoryFromStore(store), 'region');

    // then
    expect(region).toMatchObject(dataRegionsMocked[0]);
  });
});
