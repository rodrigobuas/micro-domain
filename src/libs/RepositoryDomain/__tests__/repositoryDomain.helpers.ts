import MockAdapter from 'axios-mock-adapter';
import { List } from 'immutable';

import { repositoryReducer, repositorySaga } from '../repositoryDomain.domain';
import { getLoading, getList, getMeta, getSelected } from '../repositoryDomain.store';
import { StoreTestType, createStore } from '../../../__tests__/helpers';
import { restClient } from '../repositoryDomain.api';
import { RepositoryItem, RepositoryState } from '../repositoryDomain.types';

export const axiosMock = new MockAdapter(restClient.connector);

export const createRepositoryStore = (): StoreTestType<any> =>
  createStore({ repository: repositoryReducer }, [repositorySaga]);

export const getRepositoryFromStore = (store: RepositoryItem): RepositoryState => store.getState().repository;

export const getLoadingFromStore = (store: RepositoryItem, key: string): boolean =>
  getLoading(getRepositoryFromStore(store), key);

export const getListFromStore = (store: RepositoryItem, key: string): List<RepositoryItem> =>
  getList(getRepositoryFromStore(store), key);

export const getMetaFromStore = (store: RepositoryItem, key: string): List<RepositoryItem> =>
  getMeta(getRepositoryFromStore(store), key);

export const getSelectedFromStore = (store: RepositoryItem, key: string): List<RepositoryItem> =>
  getSelected(getRepositoryFromStore(store), key);
