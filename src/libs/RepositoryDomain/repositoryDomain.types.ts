import immutable from 'immutable';

export const REPOSITORY_NAMESPACE = 'pad/repository';

export const REPOSITORY_SET_LIST = `${REPOSITORY_NAMESPACE}/SET_LIST`;
export const REPOSITORY_FETCH = `${REPOSITORY_NAMESPACE}/FETCH`;
export const REPOSITORY_DELETE = `${REPOSITORY_NAMESPACE}/DELETE`;
export const REPOSITORY_REMOVE = `${REPOSITORY_NAMESPACE}/REMOVE`;
export const REPOSITORY_EDIT = `${REPOSITORY_NAMESPACE}/EDIT`;
export const REPOSITORY_UPDATE = `${REPOSITORY_NAMESPACE}/UPDATE`;
export const REPOSITORY_ADD = `${REPOSITORY_NAMESPACE}/ADD`;
export const REPOSITORY_CREATE = `${REPOSITORY_NAMESPACE}/CREATE`;
export const REPOSITORY_EDIT_LIST = `${REPOSITORY_NAMESPACE}/EDIT_LIST`;
export const REPOSITORY_UPDATE_LIST = `${REPOSITORY_NAMESPACE}/UPDATE_LIST`;

export const REPOSITORY_SET_SELECTED = `${REPOSITORY_NAMESPACE}/SET_SELECTED`;
export const REPOSITORY_FETCH_SELECTED = `${REPOSITORY_NAMESPACE}/FETCH_SELECTED`;
export const REPOSITORY_UPDATE_SELECTED = `${REPOSITORY_NAMESPACE}/UPDATE_SELECTED`;

export type RepositoryItem = Record<string, any>;
export type RepositoryMetaItem = Record<string, any>;
export type RepositorySelectedItem = Record<string, any>;

export type RepositoryListItem = immutable.Map<string, RepositoryItem[]>;

export type RepositoryState = immutable.Record<{
  loading: immutable.Map<string, boolean>;
  lists: RepositoryListItem;
  meta: RepositoryListItem;
  selected: immutable.Map<string, RepositorySelectedItem>;
}>;
