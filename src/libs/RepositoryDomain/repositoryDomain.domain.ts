import merge from 'lodash.merge';

import { Domain, DomainOptions } from '../DomainStore';
import {
  REPOSITORY_SET_LIST,
  REPOSITORY_FETCH,
  REPOSITORY_DELETE,
  REPOSITORY_REMOVE,
  REPOSITORY_EDIT,
  REPOSITORY_UPDATE,
  REPOSITORY_ADD,
  REPOSITORY_CREATE,
  REPOSITORY_SET_SELECTED,
  REPOSITORY_FETCH_SELECTED,
  REPOSITORY_UPDATE_SELECTED,
  REPOSITORY_EDIT_LIST,
  REPOSITORY_UPDATE_LIST,
} from './repositoryDomain.types';
import {
  repositoryInitialState,
  setList,
  deleteItem,
  addItem,
  editItem,
  setLoading,
  setSelected,
  editList,
} from './repositoryDomain.store';
import {
  getListSaga,
  removeItemSaga,
  createItemSaga,
  updateItemSaga,
  getSelectedSaga,
  updateSelectedItemSaga,
  updateListSaga,
} from './repositoryDomain.saga';

const DEFAULT_REPOSITORY_PROPS = {
  initialState: repositoryInitialState,
  actions: {
    [REPOSITORY_SET_LIST]: {
      payload: ({ list, key, meta }) => ({ list, key, meta }),
      reducer: (state, { payload: { list, key, meta } }) => setList(state, key, list, meta),
    },
    [REPOSITORY_FETCH]: {
      payload: ({ endpoint, key, include, filters = {} }) => ({ endpoint, key, include, filters }),
      saga: getListSaga,
    },
    [REPOSITORY_DELETE]: {
      payload: ({ id, key }) => ({ id, key }),
      reducer: (state, { payload: { id, key } }) => deleteItem(state, key, id),
    },
    [REPOSITORY_REMOVE]: {
      payload: ({ endpoint, key, id, options = {} }) => ({ endpoint, key, id, options }),
      saga: removeItemSaga,
    },
    [REPOSITORY_ADD]: {
      payload: ({ data, key }) => ({ data, key }),
      reducer: (state, { payload: { data, key } }) => addItem(state, key, data),
    },
    [REPOSITORY_CREATE]: {
      payload: ({ endpoint, key, data, options = {} }) => ({ endpoint, key, data, options }),
      saga: createItemSaga,
    },
    [REPOSITORY_EDIT]: {
      payload: ({ data, key }) => ({ data, key }),
      reducer: (state, { payload: { data, key } }) => editItem(state, key, data),
    },
    [REPOSITORY_UPDATE]: {
      payload: ({ endpoint, key, data, options = {}, method }) => ({ endpoint, key, data, options, method }),
      saga: updateItemSaga,
    },
    [REPOSITORY_EDIT_LIST]: {
      payload: ({ data, key }) => ({ data, key }),
      reducer: (state, { payload: { data, key } }) => editList(state, key, data),
    },
    [REPOSITORY_UPDATE_LIST]: {
      payload: ({ endpoint, key, data, options = {}, method }) => ({ endpoint, key, data, options, method }),
      saga: updateListSaga,
    },
    [REPOSITORY_SET_SELECTED]: {
      payload: ({ entity, key }) => ({ entity, key }),
      reducer: (state, { payload: { entity, key } }) => setSelected(state, key, entity),
    },
    [REPOSITORY_FETCH_SELECTED]: {
      payload: ({ endpoint, key, include, filters = {} }) => ({ endpoint, key, include, filters }),
      saga: getSelectedSaga,
    },
    [REPOSITORY_UPDATE_SELECTED]: {
      payload: ({ endpoint, key, data, options = {}, method }) => ({ endpoint, key, data, options, method }),
      saga: updateSelectedItemSaga,
    },
  },
  sagaControllers: {
    start: (state, { payload: { key } }) => setLoading(state, key, true),
    success: (state, { payload: { key } }) => setLoading(state, key, false),
    error: (state, { payload: { key } }) => setLoading(state, key, false),
  },
};

export class RepositoryDomain<S> extends Domain<S> {
  constructor(props?: DomainOptions<S>) {
    const finalProp = merge({}, DEFAULT_REPOSITORY_PROPS, props);
    super(finalProp);
  }
}

export const repositoryDomain = new RepositoryDomain();

export const repositoryReducer = repositoryDomain.makeReducer();

export const repositorySaga = repositoryDomain.makeSaga();
