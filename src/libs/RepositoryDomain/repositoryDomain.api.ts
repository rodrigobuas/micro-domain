import { Method } from 'axios';

import { RestClient } from '../RestClient';

export const restClient = new RestClient({
  baseURL: '',
  tries: 1,
  dataOnly: true,
});

export const fetchList = (endpoint: string, include: string, filters: Record<string, string>): Promise<unknown> =>
  restClient.request({
    url: endpoint,
    method: 'get',
    params: { include, ...filters },
  });

export const removeItem = (endpoint: string, id: string, options?: Record<string, string>): Promise<unknown> =>
  restClient.request({
    url: `${endpoint}/${id}`,
    method: 'delete',
    params: options,
  });

export const createItem = (
  endpoint: string,
  data: Record<string, any>,
  options?: Record<string, string>,
): Promise<unknown> =>
  restClient.request({
    url: `${endpoint}`,
    method: 'post',
    params: options,
    data,
  });

export const updateItem = (
  endpoint: string,
  data: Record<string, any>,
  options?: Record<string, string>,
  method?: Method,
): Promise<unknown> =>
  restClient.request({
    url: `${endpoint}/${data.id}`,
    method: method || 'patch',
    params: options,
    data,
  });

export const updateList = (
  endpoint: string,
  data: Record<string, any>[],
  options?: Record<string, string>,
  method?: Method,
): Promise<unknown> =>
  restClient.request({
    url: endpoint,
    method: method || 'patch',
    params: options,
    data,
  });
