import { Method } from 'axios';
import { put, call } from '@redux-saga/core/effects';
import { SagaIterator } from 'redux-saga';

import { createItem, fetchList, removeItem, updateItem, updateList } from './repositoryDomain.api';
import {
  REPOSITORY_SET_LIST,
  REPOSITORY_ADD,
  REPOSITORY_DELETE,
  REPOSITORY_EDIT,
  REPOSITORY_SET_SELECTED,
  REPOSITORY_EDIT_LIST,
} from './repositoryDomain.types';

export interface RepositorySagaOptions {
  endpoint?: string;
  include?: string;
  key?: string;
  data?: Record<string, string>;
  id?: string;
  filters?: Record<string, string>;
  options?: Record<string, string>;
  method?: Method;
}

export function* getListSaga({
  payload: { endpoint, include, key, filters },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  const response = yield call(fetchList, endpoint, include, filters);
  const { data, total, limit, offset } = response;
  yield put({ type: REPOSITORY_SET_LIST, payload: { key, list: data || response, meta: { total, limit, offset } } });
}

export function* createItemSaga({
  payload: { endpoint, data, key, options },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  const newValue = yield call(createItem, endpoint, data, options);
  yield put({ type: REPOSITORY_ADD, payload: { key, data: newValue } });
}

export function* removeItemSaga({
  payload: { endpoint, id, key, options },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  yield call(removeItem, endpoint, id, options);
  yield put({ type: REPOSITORY_DELETE, payload: { id, key } });
}

export function* updateItemSaga({
  payload: { endpoint, data, key, options, method },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  const newValue = yield call(updateItem, endpoint, data, options, method);
  yield put({ type: REPOSITORY_EDIT, payload: { key, data: newValue } });
}

export function* updateListSaga({
  payload: { endpoint, data, key, options, method },
}: Record<
  string,
  {
    endpoint?: string;
    include?: string;
    key?: string;
    data?: Record<string, string>[];
    id?: string;
    filters?: Record<string, string>;
    options?: Record<string, string>;
    method?: Method;
  }
>): SagaIterator {
  const newValue = yield call(updateList, endpoint, data, options, method);
  yield put({ type: REPOSITORY_EDIT_LIST, payload: { key, data: newValue } });
}

export function* getSelectedSaga({
  payload: { endpoint, include, key, filters },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  const response = yield call(fetchList, endpoint, include, filters);
  const { data } = response;
  yield put({ type: REPOSITORY_SET_SELECTED, payload: { key, entity: (data && data[0]) || response } });
}

export function* updateSelectedItemSaga({
  payload: { endpoint, data, key, options, method },
}: Record<string, RepositorySagaOptions>): SagaIterator {
  const response = yield call(updateItem, endpoint, data, options, method);
  const entity = response.data && response.data[0];
  yield put({
    type: REPOSITORY_SET_SELECTED,
    payload: { key, entity: entity || response },
  });
}
