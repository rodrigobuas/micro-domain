import { Record as ImmutableRecord, Map as ImmutableMap, List } from 'immutable';

import { RepositoryState, RepositoryItem, RepositoryMetaItem } from './repositoryDomain.types';

const RepositoryStateFactory = ImmutableRecord({
  loading: ImmutableMap(),
  lists: ImmutableMap(),
  meta: ImmutableMap(),
  selected: ImmutableMap(),
});

export const repositoryInitialState = RepositoryStateFactory();

export function setLoading(state: RepositoryState, key: string, value: boolean): RepositoryState {
  return state.setIn(['loading', key], value);
}

export function getLoading(state: RepositoryState, key: string): boolean {
  return state.getIn(['loading', key]);
}

export function getList<T = RepositoryItem>(state: RepositoryState, key: string): List<T> {
  return state.getIn(['lists', key]);
}

export function getMeta<T = RepositoryMetaItem>(state: RepositoryState, key: string): T {
  return state.getIn(['meta', key]);
}

export function getSelected<T = RepositoryMetaItem>(state: RepositoryState, key: string): T {
  return state.getIn(['selected', key]);
}

export function setSelected(state: RepositoryState, key: string, entity: Record<string, any>): RepositoryState {
  return state.setIn(['selected', key], entity);
}

export function setList(
  state: RepositoryState,
  key: string,
  list: Array<any>,
  meta: Record<string, any>,
): RepositoryState {
  return state.setIn(['lists', key], List(list)).setIn(['meta', key], meta);
}

export function getItem<T = RepositoryItem>(
  state: RepositoryState,
  key: string,
  keyItem: string,
  valueItem: string,
): T {
  const list = state.getIn(['lists', key]);
  if (!list) {
    return null;
  }

  const entry = list.findEntry((item) => item[keyItem] === valueItem);

  if (!entry) {
    return null;
  }

  return entry[1];
}

function findIndex(state: RepositoryState, key: string, itemId: string): number {
  return getList(state, key).findIndex(({ id }) => id === itemId);
}

export function deleteItem(state: RepositoryState, key: string, itemId: string): RepositoryState {
  const index = findIndex(state, key, itemId);
  if (index < 0) {
    return state;
  }
  return state.removeIn(['lists', key, index]);
}

export function addItem(state: RepositoryState, key: string, data: RepositoryItem): RepositoryState {
  const index = findIndex(state, key, data.id);

  if (index > 0) {
    return state;
  }

  return state.mergeIn(['lists', key], data);
}

export function editItem(state: RepositoryState, key: string, data: RepositoryItem): RepositoryState {
  const index = findIndex(state, key, data.id);
  if (index < 0) {
    return state;
  }

  return state.setIn(['lists', key, index], data);
}

export function editList(state: RepositoryState, key: string, data: RepositoryItem[]): RepositoryState {
  const stateEdited = getList(state, key).toJS();
  for (let i = 0; i < data.length; i++) {
    const index = findIndex(state, key, data[i].id);
    stateEdited[index] = { ...stateEdited[index], ...data[i] };
  }

  return state.setIn(['lists', key], List(stateEdited));
}
