import find from 'lodash.find';
import get from 'lodash.get';
import merge from 'lodash.merge';

type EnvConfigRecords = Record<string, string | number | boolean>;

interface EnvOptions {
  id?: string;
  url?: string;
}

function includesTerm(url: string, term: string | number | boolean) {
  if (!url || !term) {
    return false;
  }
  return url.includes(term.toString());
}

export class EnvConfig {
  public configs: Record<string, EnvConfigRecords>;

  constructor(configs?: Record<string, EnvConfigRecords>) {
    this.configs = configs || {};
  }

  getConfig({ id, url }: EnvOptions): EnvConfigRecords {
    const specificConfig = this.getConfigByEnvId(id) || this.getConfigByUrl(url);
    const defaultConfig = this.getConfigByEnvId('default');
    if (!specificConfig && !defaultConfig) {
      return null;
    }
    return merge({}, defaultConfig, specificConfig);
  }

  getConfigByEnvId(envId: string): EnvConfigRecords {
    return envId && this.configs[envId];
  }

  getConfigByUrl(url: string): EnvConfigRecords {
    if (!url) {
      return null;
    }
    const envConfig = find(this.configs, ({ matchUrl }: EnvConfigRecords) => includesTerm(url, matchUrl));
    return envConfig;
  }

  get<T extends any>(envIdOptions: EnvOptions, key: string, defaultValue?: T): T {
    const config = this.getConfig(envIdOptions);
    return config && get(config, key, defaultValue);
  }
}
