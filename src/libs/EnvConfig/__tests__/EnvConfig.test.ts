import { EnvConfig } from '../EnvConfig';

describe('EnvConfig', () => {
  describe('constructor', () => {
    it('should instantiate an EnvConfig without configs', () => {
      // when
      const envConfig = new EnvConfig();
      // then
      expect(envConfig.configs).toEqual({});
    });

    it('should instantiate an EnvConfig with one environment', () => {
      // given
      const configs = { int: { meaningOfLife: 42 } };
      // when
      const envConfig = new EnvConfig(configs);
      // then
      expect(envConfig.configs).toEqual({ int: { meaningOfLife: 42 } });
    });
  });

  describe('getConfig', () => {
    it('should get int config by id', () => {
      // given
      const envConfig = new EnvConfig({ int: { meaningOfLife: 42 } });
      // when
      const intConfig = envConfig.getConfig({ id: 'int' });
      // then
      expect(intConfig).toBeDefined();
      expect(intConfig).toEqual({ meaningOfLife: 42 });
    });

    it('should get int config by url', () => {
      // given
      const envConfig = new EnvConfig({ int: { meaningOfLife: 42, matchUrl: 'test.com' } });
      // when
      const intConfig = envConfig.getConfig({ url: 'http://www.test.com' });
      // then
      expect(intConfig).toBeDefined();
      expect(intConfig).toEqual({ meaningOfLife: 42, matchUrl: 'test.com' });
    });

    it('should not get a config using url', () => {
      // given
      const envConfig = new EnvConfig({ int: { meaningOfLife: 42 } });
      // when
      const intConfig = envConfig.getConfig({ url: 'http://www.test.com' });
      // then
      expect(intConfig).toBeFalsy();
    });

    it('should get a default config', () => {
      // given
      const envConfig = new EnvConfig({ default: { meaningOfLife: 42 } });
      // when
      const intConfig = envConfig.getConfig({ id: 'nonEnv' });
      // then
      expect(intConfig).toEqual({ meaningOfLife: 42 });
    });
  });

  describe('get', () => {
    it('should not get a non existent configuration', () => {
      // given
      const envConfig = new EnvConfig({ int: { meaningOfLife: 42 } });
      // when
      const configValue = envConfig.get({ id: 'int' }, 'nonExistentConfiguration');
      // then
      expect(configValue).not.toBeDefined();
    });

    it('should get the meaning of life from int', () => {
      // given
      const envConfig = new EnvConfig({ int: { meaningOfLife: 42 } });
      // when
      const configValue = envConfig.get({ id: 'int' }, 'meaningOfLife');
      // then
      expect(configValue).toBe(42);
    });
  });
});
