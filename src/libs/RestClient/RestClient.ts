import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

function hasMore(count: number, total: number, offset = 0): boolean {
  if (!count) {
    return false;
  }

  return total > count + offset;
}

function readResponseTime(responseTime: string): number {
  return responseTime ? parseInt(responseTime.replace('ms', ''), 10) : 0;
}

const readTotalCount = (total: string): number => parseInt(total, 10);

interface MetaResponse {
  duration: number;
  total: number;
  count: number;
}

type ResponseReader = (response: AxiosResponse) => MetaResponse;

const readResponseMetainfo: ResponseReader = ({ data, headers }) => {
  return {
    duration: readResponseTime(headers['x-response-time']),
    total: readTotalCount(headers['x-total-count']),
    count: data && data.length,
  };
};

export interface RestClientOptions {
  baseURL: string;
  timeout?: number;
  headers?: Record<string, any>;
  intervalFactor?: number;
  tries?: number;
  dataOnly?: boolean;
  readResponseInfo?: ResponseReader;
}

export interface StreamOptions {
  url: string;
  params: Record<string, any>;
  limit?: number;
  offset?: number;
}

export interface StreamResponse {
  data: any;
  next?: () => Promise<StreamResponse>;
}

export class RestClient {
  private readonly intervalFactor: number;
  private readonly tries: number;
  private readonly dataOnly: boolean;
  private readonly readResponseInfo: ResponseReader;
  public readonly connector: AxiosInstance;

  constructor({
    baseURL,
    timeout = 0,
    headers = {},
    intervalFactor = 0.5,
    tries = 3,
    dataOnly = false,
    readResponseInfo,
  }: RestClientOptions) {
    this.connector = axios.create({ baseURL, timeout, headers });
    this.intervalFactor = intervalFactor;
    this.tries = tries;
    this.dataOnly = dataOnly;
    this.readResponseInfo = readResponseInfo || readResponseMetainfo;
  }

  async stream({ url, params, limit = 10, offset = 0 }: StreamOptions): Promise<StreamResponse> {
    const chunkedParams = { ...params, limit, offset };
    const response = await this.request({ url, params: chunkedParams });
    const data = response.data;

    const metaInfo = this.readResponseInfo(response);
    if (!hasMore(metaInfo.count, metaInfo.total, offset)) {
      return { data };
    }

    const next = async () => {
      await this.sleep(metaInfo.duration);
      return this.stream({ url, params, limit, offset: offset + metaInfo.count });
    };

    return { data, next };
  }

  async fetch(url: string, params?: Record<string, any>): Promise<unknown> {
    const response = await this.request({ method: 'get', url, params });
    return response && response.data;
  }

  async request(config: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.tryRequest(config, this.tries);
  }

  async tryRequest(config: AxiosRequestConfig, tries: number): Promise<AxiosResponse> {
    let response;
    let error;
    try {
      response = await this.connector(config);
      const dataError = response && response.data && response.data.error;
      if (!dataError) {
        return this.dataOnly ? response.data : response;
      }

      error = new Error(dataError);
    } catch (e) {
      error = e;
    }

    if (error && tries <= 1) {
      throw error;
    }

    return this.tryRequest(config, tries - 1);
  }

  sleep(duration: number): Promise<NodeJS.Timeout | null> {
    if (!duration) {
      return Promise.resolve(null);
    }

    return new Promise((resolve) => setTimeout(resolve, duration * this.intervalFactor));
  }
}
