import { missingRights } from './missingRights';

export function checkUserRights(userRights?: string[], requiredRights?: string[]): boolean {
  const missingUserRights = missingRights(userRights, requiredRights);
  return missingUserRights.length === 0;
}
