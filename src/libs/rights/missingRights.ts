export function missingRights(rights?: string[], requiredRights?: string[]): string[] {
  if (!requiredRights || !requiredRights.length) {
    return [];
  }

  if (!rights || !rights.length) {
    return requiredRights;
  }

  return requiredRights.filter((right) => !rights.includes(right));
}
