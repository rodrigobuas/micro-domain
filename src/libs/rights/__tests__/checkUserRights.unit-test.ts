import { checkUserRights } from '../checkUserRights';

describe('checkUserRights', () => {
  it('should return true when no required rights', () => {
    const result = checkUserRights(['TEST']);

    expect(result).toBe(true);
  });

  it('should return true when require ADMIN and user is ADMIN', () => {
    const result = checkUserRights(['ADMIN'], ['ADMIN']);

    expect(result).toBe(true);
  });

  it('should return false when require ADMIN and user is VIEWER', () => {
    const result = checkUserRights(['VIEWER'], ['ADMIN']);

    expect(result).toBe(false);
  });

  it('should return false when require SUPERADMIN and user is ADMIN', () => {
    const result = checkUserRights(['ADMIN'], ['ADMIN', 'SUPERADMIN']);

    expect(result).toBe(false);
  });
});
