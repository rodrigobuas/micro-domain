import { missingRights } from '../missingRights';

describe('missingRights', () => {
  it('should missing any rights when require is empty', () => {
    const result = missingRights(['TEST']);

    expect(result).toEqual([]);
  });

  it('should missing any rights when require is part of user rights', () => {
    const result = missingRights(['ADMIN', 'VIEWER'], ['ADMIN']);

    expect(result).toEqual([]);
  });

  it('should return ADMIN in missing list', () => {
    const result = missingRights(['VIEWER'], ['ADMIN']);

    expect(result).toEqual(['ADMIN']);
  });

  it('should return two missing items and', () => {
    const result = missingRights(['VIEWER'], ['ADMIN', 'SUPERADMIN']);

    expect(result).toEqual(['ADMIN', 'SUPERADMIN']);
  });
});
