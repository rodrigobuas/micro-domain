import { Store } from 'redux';
import { buildStore } from '../libs/DomainStore';

export interface StoreTestType<RootState> {
  store: Store<RootState>;
  dispatchSpy: jest.SpyInstance;
}

export function createStore(reducers = {}, sagas = []): StoreTestType<any> {
  const store = buildStore({ reducers, sagas });
  const dispatchSpy = jest.spyOn(store, 'dispatch');

  return { store, dispatchSpy };
}

export const asyncStep = (): Promise<number> => new Promise((res) => setTimeout(res));
