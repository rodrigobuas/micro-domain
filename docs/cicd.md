---
title: CICD
path: /cicd
---

# CICD

The process follows the standard gitflow [Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

Gitflow Workflow is ideally suited for projects that have a scheduled release cycle. It uses two main branches to keep the history of the project :

- master : store the official release history ([continuous integration](/#continuous-deployment))
- develop : integration branch for features ([continuous integration](/#continuous-integration))

## Continuous Integration

To start a new feature development, we create a new branch from `develop` branch.

```bash
git checkout -b feat/<evolution-label> origin/develop
git push
```

Go to gitlab and create a merge request with WIP status.

Make changes, test and push small commits.

After changes done. Remove WIP status from merge request and start review process before merge it into develop branch.

A good practice is to invite other reviewers to comment and improve changes collaboratively on the merge request until its approval.

## Continuous Deployment

### Create a new release branch

Once `develop` has acquired enough features for a new release, we fork a release branch from develop.

```bash
make prepare_release
```

No new features may be added after this point into this release - only bug fixes, documentation generation, and other release-oriented tasks may go in this branch.

Using a dedicated branch to prepare releases makes it possible for one team to polish the current release while another team continues working on features for the next release.

Once it's ready to ship, we could merge the release branch into master and tagged with a version number. In addition, it should it should be merged back into develop branch.

### Approve and tag new release

On the gitlab pipeline, we will see a manual job named `tag_release`. Activate it in order to set a tag on the current version.

### Publish

On the tags gitlab pipeline, the last stage has the "deploy" job and you could activated manually.
