# Micro Domain (BETA)

[![version](https://img.shields.io/npm/v/micro-domain?color=blue&label=version)](https://www.npmjs.com/package/micro-domain)
[![downloads](https://img.shields.io/npm/dm/micro-domain?color=blue)](https://gitlab.com/rodrigobuas/micro-domain/-/commits/develop)
[![collaborators](https://img.shields.io/npm/collaborators/micro-domain?label=collaborators&color=blue)](https://gitlab.com/rodrigobuas/micro-domain/-/commits/develop)
[![lines](https://img.shields.io/tokei/lines/gitlab/rodrigobuas/micro-domain?label=lines)](https://gitlab.com/rodrigobuas/micro-domain/-/commits/develop)

A library that aims to structure and facilitates communications between front and micro services.

## 🚨 Build Health

[![pipeline](https://gitlab.com/rodrigobuas/micro-domain/badges/develop/pipeline.svg)](https://gitlab.com/rodrigobuas/micro-domain/-/commits/develop)
[![coverage](https://gitlab.com/rodrigobuas/micro-domain/badges/develop/coverage.svg)](https://gitlab.com/rodrigobuas/micro-domain/-/commits/develop)
[![quality](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=alert_status)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)

[![reliability](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![security](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=security_rating)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![bugs](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&metric=bugs)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![duplications](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![smells](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=code_smells)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)
[![debt](https://sonarcloud.io/api/project_badges/measure?project=micro-domain&branch=develop&metric=sqale_index)](https://sonarcloud.io/dashboard?id=micro-domain&branch=develop)

- [vulnerabilities audit](https://rodrigobuas.gitlab.io/micro-domain/vulnerabilities.html)
- [licenses summary](https://rodrigobuas.gitlab.io/micro-domain/licenses-summary.txt)
- [unit test coverage report](https://rodrigobuas.gitlab.io/micro-domain/coverage-unit/lcov-report/index.html)
- [integration test coverage report](https://rodrigobuas.gitlab.io/micro-domain/coverage-int/lcov-report/index.html)
- [integration tests report](https://rodrigobuas.gitlab.io/micro-domain/int-test/index.html)

## ✨ Features

- Redux pattern for domain driven
- Repository Domain
- Event Tracker
- Rest Client
- rights decorators and middlewares

## 🎧 Dev Features

- Typescript libraty compilation
- Unit tests using jest
- Code quality repports
- ESLint for static code analyse
- Prettier for code style consistency
- Editor config settings for file format consistency
- Versioned yarn as package manager
- Husky in order to control push process and commit rules
- Commit linter with standards versions following conventions of [conventional commits](https://www.conventionalcommits.org/)
- Change log automatic generation
- Version bump management based on commit categories

## ⚙️ Setup

- run the command `make setup` to setup automatically your local environment with [development tools](/docs/setup) or follow the [step by step](/docs/setup#install-development-tools).

## 🎁 How to use it

TODO: add documentations about how to use the library

## 💎 How to test it

Run `make test` in order to run all pipeline tests or run specifically a test listed on project :

- `make test_lint` : static test on src code
- `make test_dependencies` : vulnerability test on package and its dependencies
- `make test_unit` : unit test on src code

## 🚀 CI / CD

This project follows the gitflow Workflow to implement [continuous integration](/docs/cicd.md#continuous-integration) and [continuous deployement](/docs/cicd.md#continuous-deployment).

## 📖 Documentations

TODO: add documentations about how to documentation works on it

## 🏆 Quality

In order to monitoring the quality, this project is equiped with sonar configuration.<br>
You can find the sonar results in [sonar](TODO: add sonar link here).

## ♻️ Contributing

Do you want to evolve this project ? Your are more than welcome :-)<br>
Take a look in our [code of conduct to contribute](/CONTRIBUTING.md) and the process for submitting pull requests to us.
