.EXPORT_ALL_VARIABLES:

ASSET ?= micro-domain

CI_COMMIT_REF_NAME ?= $(shell git branch --show-current)
CI_COMMIT_REF_SLUG ?= $(shell git branch --show-current | sed -e 's/\//-/g')
CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)

BUILD_VERSION=${CI_COMMIT_REF_SLUG}/${CI_COMMIT_SHORT_SHA}
RELEASE_VERSION=$$(./scripts/readVersionFromPackage.sh)
VERSION_TARGET ?= patch

help:  ## print this list
	@egrep -h '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


setup:  ## setup local environment
	@echo "Installing Yarn Version Manager ..."
	curl -fsSL https://raw.githubusercontent.com/tophat/yvm/master/scripts/install.sh | bash
	@echo "Installing Yarn ..."
	yvm install
	yvm use
	@echo "Installing Node Version Manager ..."
	curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
	@echo "Installing Automatic Version Switching for NodeJs ..."
	npm install -g avn avn-nvm avn-n
	avn setup
	@echo "Installing NodeJs ..."
	nvm install
	@echo "Testing installation ..."
	node -v
	npm -v
	yarn -v


prepare_release:  ## prepare new release
	@echo "Prepare new release ..."
	git checkout develop
	git pull
	@echo "Calculate next version ..."
	yarn release --skip.commit --release-as ${VERSION_TARGET}
	@echo "Creating new branch release/${RELEASE_VERSION} ..."
	git checkout -b release/${RELEASE_VERSION}
	git add .
	git commit -am "🔧(release): ${RELEASE_VERSION}"
	@echo "Pushing new branch release/${RELEASE_VERSION} ..."
	git push


tag_release:  ## merge and tag the current release/hotfix branch onto origin/master
	@echo "Tagging a new release into master ..."
	git checkout ${CI_COMMIT_REF_NAME}
	git checkout master
	@echo "Merge ${CI_COMMIT_REF_NAME} into master ..."
	git merge ${CI_COMMIT_REF_NAME}
	@echo "Adding tag v${RELEASE_VERSION} on master ..."
	git tag v${RELEASE_VERSION}
	git push origin --tags
	@echo "Pushing master ..."
	git push origin
	@echo "Merging release preparation into develop ..."
	git checkout develop
	git merge ${CI_COMMIT_REF_NAME}
	@echo "Pushing develop ..."
	git push origin
	@echo "Remove release branch ${CI_COMMIT_REF_NAME} ..."
	git push origin --delete ${CI_COMMIT_REF_NAME}
