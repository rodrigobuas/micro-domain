module.exports = {
  verbose: true,
  testEnvironment: 'jsdom',
  clearMocks: true,
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testRegex: '/__tests__/.*test\\.(ts|js)$',
  coverageDirectory: 'reports/coverage-unit',
  collectCoverageFrom: [
    'src/**/*.{js,ts}',
    '!src/**/*.d.ts',
    '!<rootDir>/src/index.js',
    '!<rootDir>/src/index.ts',
    '!<rootDir>/src/**/index.ts',
    '!<rootDir>/node_modules/',
    '!<rootDir>/**/__tests__/**',
    '!<rootDir>/**/__steps__/**',
  ],
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 70,
    },
  },
  testPathIgnorePatterns: ['dist'],
};
