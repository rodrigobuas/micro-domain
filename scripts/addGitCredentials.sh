git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`

echo "Setting remote as https://gitlab-ci-token:${GITLAB_TOKEN}@${url_host}"
git remote set-url origin "https://gitlab-ci-token:${GITLAB_TOKEN}@${url_host}"
